package EmpDept;

public class Employee {
    private Department dept;
    private int salary;
    private String firstName;
    private String lastName;

    public Employee(Department dept, int salary, String firstName, String lastName) {
        this.dept = dept;
        this.salary = salary;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Department getDept() {
        return dept;
    }

    public int getSalary() {
        return salary;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
