package EmpDept;

public class Department {
    private int depId;
    private String deptName;
    private String deptHead;

    public Department(int i, String name, String head) {
        this.depId = i;
        this.deptName = name;
        this.deptHead = head;
    }

    public int getDepId() {
        return depId;
    }

    public String getDeptName() {
        return deptName;
    }

    public String getDeptHead() {
        return deptHead;
    }
}
