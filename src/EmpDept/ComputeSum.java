package EmpDept;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ComputeSum {
    public static void main(String[] args) {
        Department d1 = new Department(123,"Amazon","Jeff Bezos");
        Department d2 = new Department(456,"Microsoft","Bill Gates");
        Department d3 = new Department(789,"Facebook","Mark Zuckerberg");
        Employee[] arrayOfEmps = {
                new Employee(d1,100000, "Emp1", "ln1"), // Amazon
                new Employee(d1,300000, "Emp2", "ln2"), // Amazon
                new Employee(d1,400000, "Emp3", "ln3"), // Amazon
                new Employee(d2,700000, "Emp4", "ln4"), // Microsoft
                new Employee(d2,600000, "Emp5", "ln5"), // Microsoft
                new Employee(d3,400000, "Emp6", "ln6"), // Facebook
                new Employee(d3,700000, "Emp7", "ln7"), // Facebook
                new Employee(d3,300000, "Emp8", "ln8"), // Facebook
        };
        List<Employee> listOfEmp = Arrays.asList(arrayOfEmps);
//        Integer totalSalary = listOfEmp.stream().collect(Collectors.summingInt(Employee::getSalary));
//        System.out.println(totalSalary);
        Map<Department, Integer> byDeptSalary = listOfEmp.stream()
                                                .collect(Collectors.groupingBy(Employee::getDept,
                                                Collectors.summingInt(Employee::getSalary)));

        for(Map.Entry<Department, Integer> e : byDeptSalary.entrySet()){
            System.out.println("Department : " + e.getKey().getDeptName() + "\n"
                                + "Total Salary : " + e.getValue());
        }
    }
}
