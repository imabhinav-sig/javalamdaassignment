package StudentResult;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Result {
    public static void main(String[] args) {
        Student[] arrayOfStu = {
                new Student("Stu1", 75),
                new Student("Stu2", 33),
                new Student("Stu3",21),
                new Student("Stu4", 10)
        };

        List<Student> listOfStudent = Arrays.asList(arrayOfStu);

//        Map<Boolean, List<Student>> mp = listOfStudent.stream()
//                .collect(Collectors.partitioningBy(s -> s.getMarks() >= 40));

        Map<String, List<Student>> mp = listOfStudent.stream().collect(
                Collectors.groupingBy(s -> (s.getMarks()>=40) ? "PASS" : "FAIL")
        );

        for(Map.Entry<String, List<Student>> e : mp.entrySet()){
            System.out.println(e.getKey());
            for(Student st : e.getValue()){
                System.out.println(st.getName() + " ");
            }
            System.out.println();
        }

    }
}
