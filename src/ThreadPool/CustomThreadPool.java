package ThreadPool;

import java.util.concurrent.LinkedBlockingQueue;

public class CustomThreadPool {
    private final int nThreads;
    private final PoolWorker[] threads;
    private final LinkedBlockingQueue queue;
    private boolean isShutdown;

    public CustomThreadPool(int nThreads) {
        this.nThreads = nThreads;
        queue = new LinkedBlockingQueue();
        threads = new PoolWorker[nThreads];

        for (int i = 0; i < nThreads; i++) {
            threads[i] = new PoolWorker();
            threads[i].start();
        }
        this.isShutdown = false;
    }

    public void shutDown(){
        this.isShutdown = true;
    }


    public void execute(Runnable task) {
        synchronized (queue) {
            queue.add(task);
            queue.notify();
        }
    }

    private class PoolWorker extends Thread {
        public void run() {
            try {
                while (!isShutdown || !queue.isEmpty()) {
                    Runnable r;
                    while ((r = (Runnable) queue.poll()) != null) {
                        r.run();
                    }
                    Thread.sleep(1);
                }
            } catch (RuntimeException | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
