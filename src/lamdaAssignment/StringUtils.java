package lamdaAssignment;

public class StringUtils {
        public static String betterString(String s1, String s2, TwoStringPredicate ts){
            return ts.compareString(s1,s2)?s1:s2;
        }

        public static void main(String[] args) {
            String s1 = "First String";
            String s2 = "Second String";
            String longer = StringUtils.betterString(s1, s2, (string1,string2) -> string1.length() > string2.length());
            String first = StringUtils.betterString(s1, s2, (string1, string2) -> true);
            String second = StringUtils.betterString(s1,s2, (string1, string2) -> false );
            System.out.println(longer);
            System.out.println(first); // First string if lambda is true
            System.out.println(second); // Second string if lambda is false
        }
}
