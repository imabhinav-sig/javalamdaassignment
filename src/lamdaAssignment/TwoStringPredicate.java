package lamdaAssignment;

public interface TwoStringPredicate {
    public boolean compareString(String s1, String s2);
}
